<?php

use Illuminate\Database\Seeder;
use App\Clients;
use App\Purchases;

class ClientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = Clients::updateOrCreate(['email' => 'ikrom251124@gmail.com'],[
            'phone'         => '992901901972',
        ]);
        if($client)
        {
            $purchase = Purchases::updateOrCreate(['client_id' => $client->id],
            [
                'product'   =>  'Ноутбук',
                'client_id' =>  $client->id,
                'price'     =>  500,
            ]);
        }

    }
}
