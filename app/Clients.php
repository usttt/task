<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class Clients extends Model
{
    use Notifiable;

    
    protected $filliable = ['phone', 'email'];

    public function purchases()
    {
        return $this->hasMany(Purchases::class, 'client_id', 'id');
    }
}
