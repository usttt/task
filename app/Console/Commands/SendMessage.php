<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Notifications;
use App\Clients;
use App\Purchases;
use Illuminate\Support\Facades\Notification;
use App\Notifications\SendNotification;
use Illuminate\Notifications\Notifiable;
use Illuminate\Notifications\Messages\MailMessage;

class SendMessage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'buy:event';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'list of send';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $client_header = ['id', 'phone', 'email'];
        $purchase_header = ['id', 'product', 'price'];
        $purchases = Purchases::all();

        $type = $this->ask('Выберите тип уведомлений? (email/sms)');
        if($type == 'email')
        {
            
 
            if($purchases->count() > 0) {
                $this->info('Список клиентов');
                foreach($purchases as $purchase)
                {
                    $clients =  $purchase->clients->all('id', 'phone', 'email')->toArray();    
                    $this->table($client_header, $clients);    
                    $this->info('Список покупок');
                    $this->table($purchase_header, $purchase
                            ->all('id', 'product', 'price')->toArray());
                    $this->info('Отправляется сообщение...');
                    
                    $send_notification = Notification::send($purchase->clients, new SendNotification());

                    /*
                    * Хранение уведомлений в базе
                    */

                    $notification = new Notifications();
                    $notification->type         = 'email';
                    $notification->client_id    = $purchase->clients->id;
                    $notification->phone        = $purchase->clients->phone;
                    $notification->email        = $purchase->clients->email;
                    $notification->purchase_id  = $purchase->id;
                    $notification->product      = $purchase->product;
                    $notification->price        = $purchase->price;
                    $notification->save();

                    $this->info('Сообщение успешно отправлено');             
                }
            } else {
                $this->error('Не удалось Найти запись');
            }
        }
        elseif($type == 'sms')
        {
            $this->error('Данная комманда пока недоступно');
        }
        else{
            $this->error('Неверная комманда');
        }
    }
}
