<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchases extends Model
{
    protected $filiable = ['client_id', 'price', 'product'];
    
    public function clients()
    {
        return $this->belongsTo(Clients::class, 'client_id', 'id');
    }
}
